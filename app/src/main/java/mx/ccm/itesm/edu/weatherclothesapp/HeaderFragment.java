package mx.ccm.itesm.edu.weatherclothesapp;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class HeaderFragment extends Fragment {


    public HeaderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_header,container,false);
        ButterKnife.bind(this,view);
        return view;
    }
    @BindView(R.id.min) TextView min;
    @BindView(R.id.max) TextView max;
    @BindView(R.id.temperature) TextView temperature;
    @BindView(R.id.description) TextView description;
    @BindView(R.id.clothes) ImageView clothes;

    public void setNewHeader(WeatherUnit weatherUnit){

        min.setText(weatherUnit.min.toString());
        max.setText(weatherUnit.max.toString());
        temperature.setText(weatherUnit.temperature.toString());
        description.setText(weatherUnit.weather);

        if(weatherUnit.temperature < 10.00) {
            clothes.setImageResource(R.drawable.bufanda);
        }
        else if(weatherUnit.temperature < 17.00) {
            clothes.setImageResource(R.drawable.chaqueta);
        }
        else if(weatherUnit.temperature < 25.00) {
            clothes.setImageResource(R.drawable.sueter);
        }
        else {

            clothes.setImageResource(R.drawable.bikini);
            if(weatherUnit.weather.toString().equals("Clouds")) {
                clothes.setImageResource(R.drawable.vestir);
            }
            if(weatherUnit.weather.toString().equals("Rain")) {
                clothes.setImageResource(R.drawable.blazerm);
            }
        }


    }


}
