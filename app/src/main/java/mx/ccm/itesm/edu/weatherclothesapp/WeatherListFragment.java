package mx.ccm.itesm.edu.weatherclothesapp;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class WeatherListFragment
        extends ListFragment
        implements AdapterView.OnItemClickListener{


    public WeatherListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<WeatherUnit> data = getWeatherUnits();

        WeatherListAdapter weatherListAdapter = new
                WeatherListAdapter(getActivity(),
                R.layout.item_weather, data);

        setListAdapter(weatherListAdapter);
        getListView().setOnItemClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather_list, container, false);
    }

    public static String getJSONFile (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }

    public ArrayList<WeatherUnit> getWeatherUnits(){
        ArrayList<WeatherUnit> weatherUnits = new ArrayList<WeatherUnit>();
        Context context = null;
        try {
            String jsonFile = getJSONFile("weather.json", getActivity());
            JSONObject jsonObject = new JSONObject(jsonFile);
            JSONArray jsonArray = jsonObject.getJSONArray("list");
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject object = jsonArray.getJSONObject(i);

                JSONObject temp = object.getJSONObject("temp");

                JSONArray weather = object.getJSONArray("weather");

                JSONObject weatherObject = weather.getJSONObject(0);

                WeatherUnit weatherUnit = new WeatherUnit();
                weatherUnit.temperature = temp.getDouble("day");
                weatherUnit.min = temp.getDouble("min");
                weatherUnit.max = temp.getDouble("max");
                weatherUnit.weather = weatherObject.getString("main");
                weatherUnits.add(weatherUnit);


            }



        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weatherUnits;
    }

    @Override
    public void onItemClick(AdapterView<?> parent,
                            View view, int position,
                            long id) {
        WeatherUnit weatherUnit =
                (WeatherUnit) getListAdapter().getItem(position);
        Toast.makeText(getActivity(),
                weatherUnit.weather,Toast.LENGTH_LONG ).show();

        FragmentManager fragmentManager = getFragmentManager();
        HeaderFragment headerFragment =
                (HeaderFragment) fragmentManager.findFragmentById(R.id.header);

        if(headerFragment != null) {
            headerFragment.setNewHeader(weatherUnit);
        }
    }
}
