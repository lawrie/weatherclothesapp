package mx.ccm.itesm.edu.weatherclothesapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by L00960401 on 8/25/16.
 */
public class WeatherListAdapter extends ArrayAdapter<WeatherUnit> {

    public WeatherListAdapter(Context context, int resource, List<WeatherUnit> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WeatherUnit weatherUnit = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater
                    .from(getContext())
                    .inflate(R.layout.item_weather, parent, false);

        }

        TextView temp =
                (TextView)convertView.findViewById(R.id.temp);
        TextView date =
                (TextView)convertView.findViewById(R.id.day);
        temp.setText(weatherUnit.temperature.toString());
        date.setText(weatherUnit.weather);

        return convertView;
    }


}
